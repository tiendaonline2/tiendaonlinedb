CREATE TABLE pcs (	
	codigo SERIAL PRIMARY KEY, 
    titulo VARCHAR(100),
    precio NUMERIC(18, 2),
    stock INTEGER
);

insert into pcs (titulo, precio, stock) values ('PC Gamer Ryzen 5 9600G...', 590000, 2);

select * from pcs ;
